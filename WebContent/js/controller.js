/**
 * 
 */
var address = "http://localhost:1210";

app.controller("environment", function($scope) {
	$scope.selectedEnvironment;
	$scope.setAddress = function() {
		if ($scope.selectedEnvironment == 1) {
			address = "http://localhost:1210";
		} else if ($scope.selectedEnvironment == 2) {
			address = "http://10.42.11.14:8080";
		} else {
			address = "http://30.0.231.68:8080";
		}
	}
})

app.controller("getCartUsingId", function($scope, $http) {
	$scope.request = {
		"cartId" : "f53889bd-04fe-4294-899a-5fa26f8b677a",
		"pincode" : "122002",
		"config" : {
			"promoApplicable" : "false",
			"platform" : "WEB",
			"loyaltyPointsApplicable" : "false",
			"inventoryCheckApplicable" : "false",
			"autoSellerChange" : "false",
			"deliveryDetailsUpdate" : "false"
		},
		"responseProtocol" : "PROTOCOL_JSON",
		"requestProtocol" : "PROTOCOL_JSON"
	};
	$scope.response;

	var request = $scope.request;
	$scope.getCartUsingId = function() {
		console.log($scope.request);
		$("#prettyJson1").html("");
		var req = {
			method : 'POST',
			url : address + "/service/cartMwAPIService/getCartUsingId",
			headers : {
				'Content-Type' : 'application/json'
			},
			data : request
		}

		$http(req).then(function successCallback(response) {
			console.log(response);
			$scope.response = response;
		}, function errorCallback(response) {
			console.log(response);
			$scope.response = response;
		});
	}

	$scope.addRow = function() {
		$scope.request[$scope.fieldName] = $scope.fieldValue;
		console.log($scope.request);
	}

	$scope.removeRow = function(key) {
		delete $scope.request[key];
		console.log($scope.request);
	}

	$scope.clearResponse = function() {
		$scope.response = "";
		$("#prettyJson1").html("");
	}

	$scope.prettyJson = function() {
		var pJson = JSON.stringify($scope.response.data, null, "\t");
		if (pJson && !(pJson == '""')) {
			$("#prettyJson1").html("<pre>" + pJson + "</pre>");
			$scope.response.data = "";
		}
	}

	$scope.convertJSON = function(key) {
		if ($scope.request[key].constructor === "x".constructor) {
			$scope.request[key] = JSON.parse($scope.request[key]);
		} else{
			$scope.request[key] = JSON.stringify($scope.request[key]);
		}
	}
})

app.controller("getNonFrozerCartUsingEmail", function($scope, $http) {
	$scope.request = {
		"email" : "kapil.upadhyay@snapdeal.com",
		"nextDayDeliveryEnabled" : "true",
		"pincode" : "110020",
		"sameDayDeliveryEnabled" : "true",
		"config" : {
			"promoApplicable" : "false",
			"platform" : "WEB",
			"loyaltyPointsApplicable" : "false",
			"inventoryCheckApplicable" : "false",
			"autoSellerChange" : "false",
			"deliveryDetailsUpdate" : "false"
		},
		"responseProtocol" : "PROTOCOL_JSON",
		"requestProtocol" : "PROTOCOL_JSON"
	};
	$scope.response;

	var request = $scope.request;
	$scope.getNonFrozerCartUsingEmail = function() {
		console.log($scope.request);
		$("#prettyJson2").html("");
		var req = {
			method : 'POST',
			url : address
					+ "/service/cartMwAPIService/getNonFrozenCartUsingEmail",
			headers : {
				'Content-Type' : 'application/json'
			},
			data : request
		}

		$http(req).then(function successCallback(response) {
			console.log(response);
			$scope.response = response;
		}, function errorCallback(response) {
			console.log(response)
		});
	}
	$scope.addRow = function() {
		$scope.request[$scope.fieldName] = $scope.fieldValue;
		console.log($scope.request);
	}

	$scope.removeRow = function(key) {
		delete $scope.request[key];
		console.log($scope.request);
	}
	$scope.clearResponse = function() {
		$scope.response = "";
		$("#prettyJson2").html("");
	}

	$scope.prettyJson = function() {
		var pJson = JSON.stringify($scope.response.data, null, "\t");
		if (pJson && !(pJson == '""')) {
			$("#prettyJson2").html("<pre>" + pJson + "</pre>");
			$scope.response.data = "";
		}
	}
	$scope.convertJSON = function(key) {
		if ($scope.request[key].constructor === "x".constructor) {
			$scope.request[key] = JSON.parse($scope.request[key]);
		} else{
			$scope.request[key] = JSON.stringify($scope.request[key]);
		}
	}
})

app.controller("getCartItemsCount", function($scope, $http) {
	$scope.request = {
		"cartId" : "6c9f0649-57bf-46d7-8378-30101f7e77da",
		"emailId" : "kapil.upadhyay@snapdeal.com",
		"responseProtocol" : "PROTOCOL_JSON",
		"requestProtocol" : "PROTOCOL_JSON"
	};
	$scope.getCartItemsCountResponse;

	var request = $scope.request;
	$scope.getCartItemsCount = function() {
		console.log($scope.request);
		$("#prettyJson3").html("");
		var req = {
			method : 'POST',
			url : address + "/service/cartMwAPIService/getCartItemsCount",
			headers : {
				'Content-Type' : 'application/json'
			},
			data : request
		}

		$http(req).then(function successCallback(response) {
			console.log(response);
			$scope.response = response;
		}, function errorCallback(response) {
			console.log(response)
		});
	}

	$scope.addRow = function() {
		$scope.request[$scope.fieldName] = $scope.fieldValue;
		console.log($scope.request);
	}

	$scope.removeRow = function(key) {
		delete $scope.request[key];
		console.log($scope.request);
	}
	$scope.clearResponse = function() {
		$scope.response = "";
		$("#prettyJson3").html("");
	}

	$scope.prettyJson = function() {
		var pJson = JSON.stringify($scope.response.data, null,
				"\t");
		if (pJson && !(pJson == '""')) {
			$("#prettyJson3").html("<pre>" + pJson + "</pre>");
			$scope.response.data = "";
		}
	}

	$scope.convertJSON = function(key) {
		if ($scope.request[key].constructor === "x".constructor) {
			$scope.request[key] = JSON.parse($scope.request[key]);
		} else{
			$scope.request[key] = JSON.stringify($scope.request[key]);
		}
	}

})

app.controller("freezeCart", function($scope, $http) {
	$scope.request = {
		"cartId" : "ba652607-2ead-4f09-9f87-e57bc07cb288",
		"responseProtocol" : "PROTOCOL_JSON",
		"requestProtocol" : "PROTOCOL_JSON"
	};
	$scope.response;

	var request = $scope.request;
	$scope.freezeCart = function() {
		console.log($scope.request);
		$("#prettyJson4").html("");
		var req = {
			method : 'POST',
			url : address + "/service/cartMwAPIService/freezeCart",
			headers : {
				'Content-Type' : 'application/json'
			},
			data : request
		}

		$http(req).then(function successCallback(response) {
			console.log(response);
			$scope.response = response;
		}, function errorCallback(response) {
			console.log(response)
		});
	}
	$scope.addRow = function() {
		$scope.request[$scope.fieldName] = $scope.fieldValue;
		console.log($scope.request);
	}

	$scope.removeRow = function(key) {
		delete $scope.request[key];
		console.log($scope.request);
	}

	$scope.clearResponse = function() {
		$scope.response = "";
		$("#prettyJson4").html("");
	}

	$scope.prettyJson = function() {
		var pJson = JSON.stringify($scope.response.data, null, "\t");
		if (pJson && !(pJson == '""')) {
			$("#prettyJson4").html("<pre>" + pJson + "</pre>");
			$scope.response.data = "";
		}
	}

	$scope.convertJSON = function(key) {
		if ($scope.request[key].constructor === "x".constructor) {
			$scope.request[key] = JSON.parse($scope.request[key]);
		} else{
			$scope.request[key] = JSON.stringify($scope.request[key]);
		}
	}

})

app.controller("removeItems", function($scope, $http) {
	$scope.request = {
		"cartId" : "6c9f0649-57bf-46d7-8378-30101f7e77da",
		"nextDayDeliveryEnabled" : "true",
		"pincode" : "110020",
		"sameDayDeliveryEnabled" : "true",
		"itemDetails" : [ {
			"supc" : "SDL116650124",
			"vendorCode" : "aace21"
		} ],
		"responseProtocol" : "PROTOCOL_JSON",
		"requestProtocol" : "PROTOCOL_JSON"
	};
	$scope.response;

	var request = $scope.request;
	$scope.apiHit = function() {
		console.log($scope.request);
		$("#prettyJson5").html("");
		var req = {
			method : 'POST',
			url : address + "/service/cartMwAPIService/removeItem",
			headers : {
				'Content-Type' : 'application/json'
			},
			data : request
		}

		$http(req).then(function successCallback(response) {
			console.log(response);
			$scope.response = response;
		}, function errorCallback(response) {
			console.log(response)
		});
	}
	$scope.addRow = function() {
		$scope.request[$scope.fieldName] = $scope.fieldValue;
		console.log($scope.request);
	}

	$scope.removeRow = function(key) {
		delete $scope.request[key];
		console.log($scope.request);
	}
	$scope.clearResponse = function() {
		$scope.response = "";
		$("#prettyJson5").html("");
	}

	$scope.prettyJson = function() {
		var pJson = JSON.stringify($scope.response.data, null, "\t");
		if (pJson && !(pJson == '""')) {
			$("#prettyJson5").html("<pre>" + pJson + "</pre>");
			$scope.response.data = "";
		}
	}

	$scope.convertJSON = function(key) {
		if ($scope.request[key].constructor === "x".constructor) {
			$scope.request[key] = JSON.parse($scope.request[key]);
		} else{
			$scope.request[key] = JSON.stringify($scope.request[key]);
		}
	}

})

app.controller("addUserDetailsWithCart", function($scope, $http) {
	$scope.request = {
		"email" : "kapil.upadhyay@snapdeal.com",
		"cartId" : "41bee0c4-cb4b-47e4-a308-23a979d514a1",
		"mobile" : "9739589632",
		"responseProtocol" : "PROTOCOL_JSON",
		"requestProtocol" : "PROTOCOL_JSON"
	};
	$scope.response;

	var request = $scope.request;
	$scope.apiHit = function() {
		console.log($scope.request);
		$("#prettyJson6").html("");
		var req = {
			method : 'POST',
			url : address + "/service/cartMwAPIService/addUserDetailsWithCart",
			headers : {
				'Content-Type' : 'application/json'
			},
			data : request
		}

		$http(req).then(function successCallback(response) {
			console.log(response);
			$scope.response = response;
		}, function errorCallback(response) {
			console.log(response)
		});
	}
	$scope.addRow = function() {
		$scope.request[$scope.fieldName] = $scope.fieldValue;
		console.log($scope.request);
	}

	$scope.removeRow = function(key) {
		delete $scope.request[key];
		console.log($scope.request);
	}
	$scope.clearResponse = function() {
		$scope.response = "";
		$("#prettyJson6").html("");
	}

	$scope.prettyJson = function() {
		var pJson = JSON.stringify($scope.response.data, null, "\t");
		if (pJson && !(pJson == '""')) {
			$("#prettyJson6").html("<pre>" + pJson + "</pre>");
			$scope.response.data = "";
		}
	}
	$scope.convertJSON = function(key) {
		if ($scope.request[key].constructor === "x".constructor) {
			$scope.request[key] = JSON.parse($scope.request[key]);
		} else{
			$scope.request[key] = JSON.stringify($scope.request[key]);
		}
	}
})

app.controller("insertItems", function($scope, $http) {
	$scope.request = {
		"cartId" : "",
		"nextDayDeliveryEnabled" : "true",
		"pincode" : "110020",
		"sameDayDeliveryEnabled" : "true",
		"config" : {
			"promoApplicable" : "false",
			"platform" : "WEB",
			"loyaltyPointsApplicable" : "false",
			"inventoryCheckApplicable" : "false",
			"autoSellerChange" : "false",
			"deliveryDetailsUpdate" : "false"
		},
		"mobileNumber" : "9739589632",
		"itemDetailsList" : [ {
			"supc" : "SDL575002139",
			"vendorCode" : "971850",
			"quantity" : "1",
			"catalogId" : "579697531"
		}, {
			"supc" : "SDL311184065",
			"vendorCode" : "971850",
			"quantity" : "1",
			"catalogId" : "2043122185"
		} ],
		"responseProtocol" : "PROTOCOL_JSON",
		"requestProtocol" : "PROTOCOL_JSON"
	};
	$scope.response;

	var request = $scope.request;
	$scope.apiHit = function() {
		console.log($scope.request);
		$("#prettyJson7").html("");
		var req = {
			method : 'POST',
			url : address + "/service/cartMwAPIService/insertItems",
			headers : {
				'Content-Type' : 'application/json'
			},
			data : request
		}

		$http(req).then(function successCallback(response) {
			console.log(response);
			$scope.response = response;
		}, function errorCallback(response) {
			console.log(response)
		});
	}
	$scope.addRow = function() {
		$scope.request[$scope.fieldName] = $scope.fieldValue;
		console.log($scope.request);
	}

	$scope.removeRow = function(key) {
		delete $scope.request[key];
		console.log($scope.request);
	}
	$scope.clearResponse = function() {
		$scope.response = "";
		$("#prettyJson7").html("");
	}

	$scope.prettyJson = function() {
		var pJson = JSON.stringify($scope.response.data, null, "\t");
		if (pJson && !(pJson == '""')) {
			$("#prettyJson7").html("<pre>" + pJson + "</pre>");
			$scope.response.data = "";
		}
	}
	$scope.convertJSON = function(key) {
		if ($scope.request[key].constructor === "x".constructor) {
			$scope.request[key] = JSON.parse($scope.request[key]);
		} else{
			$scope.request[key] = JSON.stringify($scope.request[key]);
		}
	}
})

app.controller("associateOrderInfoWithCart", function($scope, $http) {
	$scope.request = {
		"cartId" : "7312ec98-9ab2-45ba-9e93-59cf6073b58e",
		"orderInfo" : {
			"orderCode" : "453454334",
			"addressId" : 926,
			"productType" : "STANDARD"
		},
		"storeFront" : {
			"code" : "SDL"
		},
		"pincode" : "122001",
		"basketType" : "SD_INSTANT",
		"responseProtocol" : "PROTOCOL_JSON",
		"requestProtocol" : "PROTOCOL_JSON"
	};
	$scope.response;

	var request = $scope.request;
	$scope.apiHit = function() {
		console.log($scope.request);
		$("#prettyJson8").html("");
		var req = {
			method : 'POST',
			url : address
					+ "/service/cartMwAPIService/associateOrderInfoWithCart",
			headers : {
				'Content-Type' : 'application/json'
			},
			data : request
		}

		$http(req).then(function successCallback(response) {
			console.log(response);
			$scope.response = response;
		}, function errorCallback(response) {
			console.log(response)
		});
	}
	$scope.addRow = function() {
		$scope.request[$scope.fieldName] = $scope.fieldValue;
		console.log($scope.request);
	}

	$scope.removeRow = function(key) {
		delete $scope.request[key];
		console.log($scope.request);
	}
	$scope.clearResponse = function() {
		$scope.response = "";
		$("#prettyJson8").html("");
	}

	$scope.prettyJson = function() {
		var pJson = JSON.stringify($scope.response.data, null, "\t");
		if (pJson && !(pJson == '""')) {
			$("#prettyJson8").html("<pre>" + pJson + "</pre>");
			$scope.response.data = "";
		}
	}

	$scope.convertJSON = function(key) {
		if ($scope.request[key].constructor === "x".constructor) {
			$scope.request[key] = JSON.parse($scope.request[key]);
		} else{
			$scope.request[key] = JSON.stringify($scope.request[key]);
		}
	}
})

app.controller("updateCartItemQuantity", function($scope, $http) {
	$scope.request = {
		"cartId" : "4c3e64b7-654f-44a9-836b-2e0196df6e49",
		"nextDayDeliveryEnabled" : "true",
		"itemDetails" : [ {
			"supc" : "SDL679214039",
			"vendorCode" : "fe9962",
			"quantity" : "2",
			"bundleId" : "SDL051328309"
		} ],
		"pincode" : "110020",
		"sameDayDeliveryEnabled" : "true",
		"config.promoapplicable" : "true",
		"responseProtocol" : "PROTOCOL_JSON",
		"requestProtocol" : "PROTOCOL_JSON"
	};
	$scope.response;

	var request = $scope.request;
	$scope.apiHit = function() {
		console.log($scope.request);
		$("#prettyJson9").html("");
		var req = {
			method : 'POST',
			url : address + "/service/cartMwAPIService/updateCartItemQuantity",
			headers : {
				'Content-Type' : 'application/json'
			},
			data : request
		}

		$http(req).then(function successCallback(response) {
			console.log(response);
			$scope.response = response;
		}, function errorCallback(response) {
			console.log(response)
		});
	}
	$scope.addRow = function() {
		$scope.request[$scope.fieldName] = $scope.fieldValue;
		console.log($scope.request);
	}

	$scope.removeRow = function(key) {
		delete $scope.request[key];
		console.log($scope.request);
	}
	$scope.clearResponse = function() {
		$scope.response = "";
		$("#prettyJson9").html("");
	}

	$scope.prettyJson = function() {
		var pJson = JSON.stringify($scope.response.data, null, "\t");
		if (pJson && !(pJson == '""')) {
			$("#prettyJson9").html("<pre>" + pJson + "</pre>");
			$scope.response.data = "";
		}
	}

	$scope.convertJSON = function(key) {
		if ($scope.request[key].constructor === "x".constructor) {
			$scope.request[key] = JSON.parse($scope.request[key]);
		} else{
			$scope.request[key] = JSON.stringify($scope.request[key]);
		}
	}
})

app.controller("updateBasketItemQuantity", function($scope, $http) {
	$scope.request = {
		"cookieCartId" : "08113be4-7882-41d4-9e4d-3a793e1d6e75",
		"userEmail" : "kapil.upadhyay@snapdeal.com",
		"itemDetailsList" : [ {
			"supc" : "BOK063676149",
			"vendorCode" : "b82f06",
			"quantity" : "3"
		} ],
		"pincode" : "110020",
		"platform" : "WEB",
		"basketType" : "SD_INSTANT",
		"responseProtocol" : "PROTOCOL_JSON",
		"requestProtocol" : "PROTOCOL_JSON"
	};
	$scope.response;

	var request = $scope.request;
	$scope.apiHit = function() {
		console.log($scope.request);
		$("#prettyJson10").html("");
		var req = {
			method : 'POST',
			url : address
					+ "/service/cartMwAPIService/updateBasketItemQuantity",
			headers : {
				'Content-Type' : 'application/json'
			},
			data : request
		}

		$http(req).then(function successCallback(response) {
			console.log(response);
			$scope.response = response;
		}, function errorCallback(response) {
			console.log(response)
		});
	}
	$scope.addRow = function() {
		$scope.request[$scope.fieldName] = $scope.fieldValue;
		console.log($scope.request);
	}

	$scope.removeRow = function(key) {
		delete $scope.request[key];
		console.log($scope.request);
	}
	$scope.clearResponse = function() {
		$scope.response = "";
		$("#prettyJson10").html("");
	}

	$scope.prettyJson = function() {
		var pJson = JSON.stringify($scope.response.data, null, "\t");
		if (pJson && !(pJson == '""')) {
			$("#prettyJson10").html("<pre>" + pJson + "</pre>");
			$scope.response.data = "";
		}
	}

	$scope.convertJSON = function(key) {
		if ($scope.request[key].constructor === "x".constructor) {
			$scope.request[key] = JSON.parse($scope.request[key]);
		} else{
			$scope.request[key] = JSON.stringify($scope.request[key]);
		}
	}
})

app.controller("insertBasketItems", function($scope, $http) {
	$scope.request = {
		"cookieCartId" : "null",
		"platform" : "APP",
		"itemDetailsList" : [ {
			"supc" : "SDL518079357",
			"vendorCode" : "e16a2a",
			"quantity" : "1",
			"catalogId" : "142556590557831"
		} ],
		"responseProtocol" : "PROTOCOL_JSON",
		"requestProtocol" : "PROTOCOL_JSON",
		"pincode" : "122011",
		"basketType" : "SD_INSTANT",
		"userEmail" : "kapil.upadhyay@snapdeal.com"
	};
	$scope.response;

	var request = $scope.request;
	$scope.apiHit = function() {
		console.log($scope.request);
		$("#prettyJson11").html("");
		var req = {
			method : 'POST',
			url : address + "/service/cartMwAPIService/insertBasketItems",
			headers : {
				'Content-Type' : 'application/json'
			},
			data : request
		}

		$http(req).then(function successCallback(response) {
			console.log(response);
			$scope.response = response;
		}, function errorCallback(response) {
			console.log(response)
		});
	}
	$scope.addRow = function() {
		$scope.request[$scope.fieldName] = $scope.fieldValue;
		console.log($scope.request);
	}

	$scope.removeRow = function(key) {
		delete $scope.request[key];
		console.log($scope.request);
	}
	$scope.clearResponse = function() {
		$scope.response = "";
		$("#prettyJson11").html("");
	}

	$scope.prettyJson = function() {
		var pJson = JSON.stringify($scope.response.data, null, "\t");
		if (pJson && !(pJson == '""')) {
			$("#prettyJson11").html("<pre>" + pJson + "</pre>");
			$scope.response.data = "";
		}
	}

	$scope.convertJSON = function(key) {
		if ($scope.request[key].constructor === "x".constructor) {
			$scope.request[key] = JSON.parse($scope.request[key]);
		} else{
			$scope.request[key] = JSON.stringify($scope.request[key]);
		}
	}
})
