<html>
<head>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/angular.min.js"></script>
<script type="text/javascript" src="../js/module.js"></script>
<script type="text/javascript" src="../js/controller.js"></script>
<script type="text/javascript" src="../js/methods.js"></script>
<link rel="stylesheet" type="text/css" href="../css/cart.css">

</head>

<body ng-app="cart">
	<h1 class="head">
		<img class="logo" src="../images/logo.png"></img>CART PANEL
	</h1>
	<div class="container">
		<div class="page-header" ng-controller="environment"
			ng-init="selectedEnvironment=1">
			<form>
				<label class="radio-inline"><input checked type="radio"
					name="optradio" ng-model="selectedEnvironment" value="1"
					ng-click="setAddress()">Local</label> <label class="radio-inline"><input
					type="radio" name="optradio" ng-model="selectedEnvironment"
					value="2" ng-click="setAddress()">Staging</label> <label
					class="radio-inline"><input type="radio" name="optradio"
					ng-model="selectedEnvironment" value="3" ng-click="setAddress()">Production</label>

			</form>
		</div>
		<!-- getCartUsingId -->
		<div class="panel-group" id="accordion">
			<div class="panel panel-default" ng-controller="getCartUsingId">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapse1">getCartUsingId</a>
					</h4>
				</div>
				<div id="collapse1" class="panel-collapse collapse in">
					<div class="panel-body">
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="(key, val) in request">
									<td style="border: none">{{key}}</td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="request[key]"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-retweet"
											ng-click="convertJSON(key)"></div></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-minus" ng-click="removeRow(key)"></div></td>
								</tr>
							</tbody>
						</table>

						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldName"></td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldValue"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-plus" ng-click="addRow()"></div></td>
								</tr>
							</tbody>
						</table>

						<div class="response">{{response.data}}</div>
						<div id="prettyJson1"></div>
						<div>
							<div class="glyphicon glyphicon-leaf" ng-click="prettyJson()"></div>
							pretty JSON
						</div>
						<div>
							<div class="glyphicon glyphicon-trash" ng-click="clearResponse()"></div>
							clear response
						</div>
						<button type="button" class="btn btn-secondary"
							ng-click="getCartUsingId()">getCartUsingID</button>
					</div>
				</div>
			</div>

			<!-- getNonFrozenCartUsingEmail -->
			<div class="panel panel-default"
				ng-controller="getNonFrozerCartUsingEmail">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapse2">getNonFrozenCartUsingEmail</a>
					</h4>
				</div>
				<div id="collapse2" class="panel-collapse collapse">
					<div class="panel-body">
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="(key, val) in request">
									<td style="border: none">{{key}}</td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="request[key]"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-retweet"
											ng-click="convertJSON(key)"></div></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-minus" ng-click="removeRow(key)"></div></td>
								</tr>
							</tbody>
						</table>
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldName"></td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldValue"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-plus" ng-click="addRow()"></div></td>
								</tr>
							</tbody>
						</table>
						<div class="response">{{response.data}}</div>
						<div id="prettyJson2"></div>
						<div>
							<div class="glyphicon glyphicon-leaf" ng-click="prettyJson()"></div>
							pretty JSON
						</div>
						<div>
							<div class="glyphicon glyphicon-trash" ng-click="clearResponse()"></div>
							clear response
						</div>
						<button type="button" class="btn btn-secondary"
							ng-click="getNonFrozerCartUsingEmail()">getNonFrozerCartUsingEmail</button>
					</div>
				</div>
			</div>

			<!-- getCartItemsCount -->
			<div class="panel panel-default" ng-controller="getCartItemsCount">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapse3">getCartItemsCount</a>
					</h4>
				</div>
				<div id="collapse3" class="panel-collapse collapse">
					<div class="panel-body">
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="(key, val) in request">
									<td style="border: none">{{key}}</td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="request[key]"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-retweet"
											ng-click="convertJSON(key)"></div></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-minus" ng-click="removeRow(key)"></div></td>
								</tr>
							</tbody>
						</table>
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldName"></td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldValue"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-plus" ng-click="addRow()"></div></td>
								</tr>
							</tbody>
						</table>


						<div class="response">{{response.data}}</div>
						<div id="prettyJson3"></div>
						<div>
							<div class="glyphicon glyphicon-leaf" ng-click="prettyJson()"></div>
							pretty JSON
						</div>
						<div>
							<div class="glyphicon glyphicon-trash" ng-click="clearResponse()"></div>
							clear response
						</div>
						<button type="button" class="btn btn-secondary"
							ng-click="getCartItemsCount()">getCartItemsCount</button>
					</div>
				</div>
			</div>

			<!-- freezeCart -->
			<div class="panel panel-default" ng-controller="freezeCart">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapse4">freezeCart</a>
					</h4>
				</div>
				<div id="collapse4" class="panel-collapse collapse">
					<div class="panel-body">
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="(key, val) in request">
									<td style="border: none">{{key}}</td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="request[key]"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-retweet"
											ng-click="convertJSON(key)"></div></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-minus" ng-click="removeRow(key)"></div></td>
								</tr>
							</tbody>
						</table>
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldName"></td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldValue"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-plus" ng-click="addRow()"></div></td>
								</tr>
							</tbody>
						</table>
						<div class="response">{{response.data}}</div>
						<div id="prettyJson4"></div>
						<div>
							<div class="glyphicon glyphicon-leaf" ng-click="prettyJson()"></div>
							pretty JSON
						</div>
						<div>
							<div class="glyphicon glyphicon-trash" ng-click="clearResponse()"></div>
							clear response
						</div>
						<button type="button" class="btn btn-secondary"
							ng-click="freezeCart()">freezeCart</button>
					</div>
				</div>
			</div>


			<!-- removeItems -->
			<div class="panel panel-default" ng-controller="removeItems">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapse5">removeItems</a>
					</h4>
				</div>
				<div id="collapse5" class="panel-collapse collapse">
					<div class="panel-body">
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="(key, val) in request">
									<td style="border: none">{{key}}</td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="request[key]"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-retweet"
											ng-click="convertJSON(key)"></div></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-minus" ng-click="removeRow(key)"></div></td>
								</tr>
							</tbody>
						</table>
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldName"></td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldValue"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-plus" ng-click="addRow()"></div></td>
								</tr>
							</tbody>
						</table>
						<div class="response">{{response.data}}</div>
						<div id="prettyJson5"></div>
						<div>
							<div class="glyphicon glyphicon-leaf" ng-click="prettyJson()"></div>
							pretty JSON
						</div>
						<div>
							<div class="glyphicon glyphicon-trash" ng-click="clearResponse()"></div>
							clear response
						</div>
						<button type="button" class="btn btn-secondary"
							ng-click="apiHit()">removeItems</button>
					</div>
				</div>
			</div>

			<!-- addUserDetailsWithCart -->
			<div class="panel panel-default"
				ng-controller="addUserDetailsWithCart">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapse6">addUserDetailsWithCart</a>
					</h4>
				</div>
				<div id="collapse6" class="panel-collapse collapse">
					<div class="panel-body">
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="(key, val) in request">
									<td style="border: none">{{key}}</td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="request[key]"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-retweet"
											ng-click="convertJSON(key)"></div></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-minus" ng-click="removeRow(key)"></div></td>
								</tr>
							</tbody>
						</table>
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldName"></td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldValue"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-plus" ng-click="addRow()"></div></td>
								</tr>
							</tbody>
						</table>
						<div class="response">{{response.data}}</div>
						<div id="prettyJson6"></div>
						<div>
							<div class="glyphicon glyphicon-leaf" ng-click="prettyJson()"></div>
							pretty JSON
						</div>
						<div>
							<div class="glyphicon glyphicon-trash" ng-click="clearResponse()"></div>
							clear response
						</div>
						<button type="button" class="btn btn-secondary"
							ng-click="apiHit()">addUserDetailsWithCart</button>
					</div>
				</div>
			</div>

			<!-- insertItems -->
			<div class="panel panel-default" ng-controller="insertItems">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapse7">insertItems</a>
					</h4>
				</div>
				<div id="collapse7" class="panel-collapse collapse">
					<div class="panel-body">
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="(key, val) in request">
									<td style="border: none">{{key}}</td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="request[key]"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-retweet"
											ng-click="convertJSON(key)"></div></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-minus" ng-click="removeRow(key)"></div></td>
								</tr>
							</tbody>
						</table>
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldName"></td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldValue"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-plus" ng-click="addRow()"></div></td>
								</tr>
							</tbody>
						</table>
						<div class="response">{{response.data}}</div>
						<div id="prettyJson7"></div>
						<div>
							<div class="glyphicon glyphicon-leaf" ng-click="prettyJson()"></div>
							pretty JSON
						</div>
						<div>
							<div class="glyphicon glyphicon-trash" ng-click="clearResponse()"></div>
							clear response
						</div>
						<button type="button" class="btn btn-secondary"
							ng-click="apiHit()">insertItems</button>
					</div>
				</div>
			</div>

			<!-- associateOrderInfoWithCart -->
			<div class="panel panel-default"
				ng-controller="associateOrderInfoWithCart">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapse8">associateOrderInfoWithCart</a>
					</h4>
				</div>
				<div id="collapse8" class="panel-collapse collapse">
					<div class="panel-body">
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="(key, val) in request">
									<td style="border: none">{{key}}</td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="request[key]"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-retweet"
											ng-click="convertJSON(key)"></div></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-minus" ng-click="removeRow(key)"></div></td>
								</tr>
							</tbody>
						</table>
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldName"></td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldValue"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-plus" ng-click="addRow()"></div></td>
								</tr>
							</tbody>
						</table>
						<div class="response">{{response.data}}</div>
						<div id="prettyJson8"></div>
						<div>
							<div class="glyphicon glyphicon-leaf" ng-click="prettyJson()"></div>
							pretty JSON
						</div>
						<div>
							<div class="glyphicon glyphicon-trash" ng-click="clearResponse()"></div>
							clear response
						</div>
						<button type="button" class="btn btn-secondary"
							ng-click="apiHit()">associateOrderInfoWithCart</button>
					</div>
				</div>
			</div>

			<!-- updateCartItemQuantity -->
			<div class="panel panel-default"
				ng-controller="updateCartItemQuantity">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapse9">updateCartItemQuantity</a>
					</h4>
				</div>
				<div id="collapse9" class="panel-collapse collapse">
					<div class="panel-body">
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="(key, val) in request">
									<td style="border: none">{{key}}</td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="request[key]"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-retweet"
											ng-click="convertJSON(key)"></div></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-minus" ng-click="removeRow(key)"></div></td>
								</tr>
							</tbody>
						</table>
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldName"></td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldValue"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-plus" ng-click="addRow()"></div></td>
								</tr>
							</tbody>
						</table>
						<div class="response">{{response.data}}</div>
						<div id="prettyJson9"></div>
						<div>
							<div class="glyphicon glyphicon-leaf" ng-click="prettyJson()"></div>
							pretty JSON
						</div>
						<div>
							<div class="glyphicon glyphicon-trash" ng-click="clearResponse()"></div>
							clear response
						</div>
						<button type="button" class="btn btn-secondary"
							ng-click="apiHit()">updateCartItemQuantity</button>
					</div>
				</div>
			</div>

			<!-- updateBasketItemQuantity -->
			<div class="panel panel-default"
				ng-controller="updateBasketItemQuantity">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapse10">updateBasketItemQuantity</a>
					</h4>
				</div>
				<div id="collapse10" class="panel-collapse collapse">
					<div class="panel-body">
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="(key, val) in request">
									<td style="border: none">{{key}}</td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="request[key]"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-retweet"
											ng-click="convertJSON(key)"></div></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-minus" ng-click="removeRow(key)"></div></td>
								</tr>
							</tbody>
						</table>
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldName"></td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldValue"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-plus" ng-click="addRow()"></div></td>
								</tr>
							</tbody>
						</table>
						<div class="response">{{response.data}}</div>
						<div id="prettyJson10"></div>
						<div>
							<div class="glyphicon glyphicon-leaf" ng-click="prettyJson()"></div>
							pretty JSON
						</div>
						<div>
							<div class="glyphicon glyphicon-trash" ng-click="clearResponse()"></div>
							clear response
						</div>
						<button type="button" class="btn btn-secondary"
							ng-click="apiHit()">updateBasketItemQuantity</button>
					</div>
				</div>
			</div>

			<!-- insertBasketItems -->
			<div class="panel panel-default" ng-controller="insertBasketItems">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion"
							href="#collapse11">insertBasketItems</a>
					</h4>
				</div>
				<div id="collapse11" class="panel-collapse collapse">
					<div class="panel-body">
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="(key, val) in request">
									<td style="border: none">{{key}}</td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="request[key]"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-retweet"
											ng-click="convertJSON(key)"></div></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-minus" ng-click="removeRow(key)"></div></td>
								</tr>
							</tbody>
						</table>
						<table class="table cstm-table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldName"></td>
									<td style="border: none"><input type="text"
										class="form-control" ng-model="fieldValue"></td>
									<td style="border: none"><div
											class="glyphicon glyphicon-plus" ng-click="addRow()"></div></td>
								</tr>
							</tbody>
						</table>
						<div class="response">{{response.data}}</div>
						<div id="prettyJson11"></div>
						<div>
							<div class="glyphicon glyphicon-leaf" ng-click="prettyJson()"></div>
							pretty JSON
						</div>
						<div>
							<div class="glyphicon glyphicon-trash" ng-click="clearResponse()"></div>
							clear response
						</div>
						<button type="button" class="btn btn-secondary"
							ng-click="apiHit()">insertBasketItems</button>
					</div>
				</div>
			</div>


		</div>
	</div>

</body>
</html>